<?php
    class Fruit{
        public $name="bacho!";
        public $color;

        function __construct($name){
            $this->name = $name;
            echo "<h1>Hello I am Fruit!!!</h1>";
        }

        function print_color(){
            echo "<h1>This Is my red!!!</h1>";
        }

        function print_name(){
            echo "<h1>My name is {$this->name}!!!</h1>";
        }
    }


    $fruit1 = new Fruit("Apple");
    $fruit1->print_color();
    $fruit1->print_name();
    echo "<hr>";

    $fruit2 = new Fruit("Banana");
    $fruit2->print_color();
    $fruit2->print_name();

?>
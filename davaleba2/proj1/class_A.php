<h1>Class A</h1>


<?php

    class A{
        
        public $a;
        public $b;

        public function method1(){
            echo "<h2>method 1</h2>";
            echo "a = ".$this->a;
            echo "<br>";
            echo "b = ".$this->b;
            }
        
        
        public function method2(){
            echo "<hr>";
            echo "<h2>method 2</h2>";
            echo "a + b = ".$this->a + $this->b;
         }

         public function method3(){
            echo "<hr>";
            echo "<h2>method 3</h2>";
            echo $this->a*$this->b;
            return $this->a*$this->b;
            
         }

    }


    $A1 = new A();
    $A1->a = 5;
    $A1->b = 8;
    $A1->method1();
    $A1->method2();
    $A1->method3();
    
?>
<hr><hr><h1>Class B</h1>


<?php

    class B{
        
        public $a;
        public $b;
        public $c;

        public function method1b(){
            echo "<h2>method 1</h2>";
            echo "a = ".$this->a;
            echo "<br>";
            echo "b = ".$this->b;
            echo "<br>";
            echo "c = ".$this->c;
            }
        
        
        public function method2b(){
            echo "<hr>";
            echo "<h2>method 2</h2>";
            if($this->a > $this->b && $this->a > $this->c){
                echo "udidesi aris = ".$this->a;
            }if($this->b > $this->a && $this->b > $this->c){
                echo "udidesi aris = ".$this->b;
            }else{
                echo "udidesi aris = ".$this->c;
            }
         }

         public function method3b(){
            echo "<hr>";
            echo "<h2>method 3</h2>";
           
            if($this->a < $this->b && $this->a < $this->c){
                echo "umciresi aris = ".$this->a;
            }if($this->b < $this->a && $this->b < $this->c){
                echo "umciresi aris = ".$this->b;
            }if($this->c < $this->a && $this->c < $this->b){
                echo "umciresi aris = ".$this->c;
            }
            echo "<br>"."RETURN";
         }

    }

    $B1 = new B();
    $B1->a = 4;
    $B1->b = 13;
    $B1->c = 69;
    $B1->method1b();
    $B1->method2b();
    $B1->method3b();

?>
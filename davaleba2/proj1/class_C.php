<hr><hr><h1>Class C</h1>


<?php

    class C{
        
        public $a;
        public $b;
        public $c;

        public function method1c(){
            echo "<h2>method 1</h2>";
            echo "a = ".$this->a;
            echo "<br>";
            echo "b = ".$this->b;
            echo "<br>";
            echo "c = ".$this->c;
            }
        
        
        public function method2c(){
            echo "<hr>";
            echo "<h2>method 2</h2>";
            $bolo = $this->a % 10;
            echo "a-s bolo cifria ".$bolo;
         }

         public function method3c(){
            echo "<hr>";
            echo "<h2>method 3</h2>";
            $pirveli = substr($this->b, 0, 1);
            echo "b-s pirveli cifria ".$pirveli;

         }
         public function method4c(){
            $jami = 0;
            echo "<hr>";
            echo "<h2>method 4</h2>";
            for ($i =0; $i<=strlen($this->c);$i++)  
            {  
             $x=$this->c%10;  
              $jami = $jami + $x;  
              $this->c=$this->c/10;  
             }  
            echo "c-s cifrta jamia ".$jami;
         }
         public function method5c(){
            echo "<hr>";
            echo "<h2>method 5</h2>";
            $c5 = $this->bolo * $this->pirveli; 
            echo $c5;
         }

         public function method6c(){
            echo "<hr>";
            echo "<h2>method 6</h2>";
            $c6 = $this->c5 + $this->jami; 
            echo $c6;
         }

    }

    $C1 = new C();
    $C1->a = 193;
    $C1->b = 662;
    $C1->c = 43392;
    $C1->method1c();
    $C1->method2c();
    $C1->method3c();
    $C1->method4c();
    $C1->method5c();
    $C1->method6c();

?>
<hr>
<h1>Page 1</h1>

<?php
    class Person{
        public $name;
        public $last_name;
        private $age = 19;
        protected $id = "123124123123";

        protected function print_php_info(){
            var_dump(phpinfo());
        }

        private function print_class_name(){
            echo "<h3>Person</h3>";
        }

        public function print_name(){
            echo $this->name;
        }

        public function print_age(){
            $this->print_class_name();
            echo $this->age;
        }

        public function get_age(){
            return $this->age;
        }  

        public function get_id(){
            return $this->id;
        }
    }

    $person1 = new Person();
    $person1->name = "Viqtoria";
    // $person1->age = 19;
    // $person1->id = "01019022312";
    $person1->print_age();
    $age2 = $person1->get_age() + 10;
    echo "<br>";
    echo "age = {$age2}";
    // $person1->print_php_info();

?>